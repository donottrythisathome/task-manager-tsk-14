package com.ushakov.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        Integer intVal;
        try {
            intVal = Integer.parseInt(SCANNER.nextLine());
            return intVal;
        }
        catch(NumberFormatException e) {
            return 0;
        }
    }

}
