package com.ushakov.tm.comparator;

import com.ushakov.tm.api.entity.IHasDateStart;

import java.util.Comparator;

public final class StartDateComparator implements Comparator<IHasDateStart> {

    private static final StartDateComparator INSTANCE = new StartDateComparator();

    private StartDateComparator() {
    }

    public static StartDateComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasDateStart o1, final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
