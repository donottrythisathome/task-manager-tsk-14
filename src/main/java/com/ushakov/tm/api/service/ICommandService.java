package com.ushakov.tm.api.service;

import com.ushakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
